//
//  UserProfile.swift
//  Hire
//
//  Created by Skyler Gast on 3/10/21.
//

import Foundation

struct UserProfile {
    var id: String
    var name: String
    var profileImage: String
    var salary: NSNumber
    var skills: [String]
    
    var fullTime: Bool
    var remote: Bool
    var travel: Bool
    
    var applied: [String]
    var rejected: [String]
}
