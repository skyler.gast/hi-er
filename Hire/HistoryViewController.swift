//
//  HistoryViewController.swift
//  Hire
//
//  Created by Skyler Gast on 1/26/21.
//

import Foundation
import UIKit

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cardView: CardView!
    
    @IBOutlet weak var companyLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    var opps: Array<Opportunity>!
    
    override func viewDidLoad() {
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.fillEqually
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing   = 16.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        opps.forEach { opp in
            let opportunityView = UIStackView()
            let companyLabel = UILabel()
            let candidateMatched = UILabel()
            let matched = UIImageView()
            
            companyLabel.text = opp.title
            candidateMatched.text = opp.matchedCandidate?.description
            
            if (opp.matchedCandidate == true) {
                matched.image = UIImage(systemName: "hand.thumbsup.fill")
            }
            if (opp.matchedCandidate == false) {
                matched.image = UIImage(systemName: "hand.thumbsdown.fill")
            }
            
            opportunityView.axis = NSLayoutConstraint.Axis.horizontal
            opportunityView.distribution = UIStackView.Distribution.fill
            
            opportunityView.addArrangedSubview(companyLabel)
            opportunityView.addArrangedSubview(matched)
            
            stackView.addArrangedSubview(opportunityView)
        }

    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
}
