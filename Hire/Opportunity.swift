//
//  Opportunity.swift
//  Hire
//
//  Created by Skyler Gast on 11/12/20.
//

import Foundation
import MapKit
import Firebase

class Opportunity: NSObject, MKAnnotation {
    let id: String?
    let title: String?
    let subtitle: String?
    var coordinate: CLLocationCoordinate2D
    let shortDesc: String?
    let desc: String?
    var logo: UIImage?
    let url: String?
    let address: String?
    
    var hidden: Bool? = false
    var matchedCandidate: Bool? = false
    var matchedEmployer: Bool? = false
    
    var skills: Array<String> = [String]()
    //let matchedWith: Array?
    
    
    init(
        id: String?,
        title: String?,
        subtitle: String?,
        coordinate: CLLocationCoordinate2D,
        shortDesc: String?,
        desc: String?,
        logo: UIImage?,
        url: String?,
        address: String?
    ) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.shortDesc = shortDesc
        self.desc = desc
        self.logo = logo
        self.url = url
        self.address = address
            
        super.init()
    }
    
    func setApplied(userProfile: UserProfile) {
        //call db - user profile -> applied -> add element Opportunity DocumentId/id
        
        //let db = Firestore.firestore()
        
    }
    
    func setRejected(userProfile: UserProfile) {
        
    }
    
}
