//
//  SuccessViewController.swift
//  Hire
//
//  Created by Skyler Gast on 1/18/21.
//

import Foundation
import UIKit

class SuccessViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIButton!
    
    var opp: Opportunity!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImage.setImage(opp.logo, for: .normal)
        
    }
    
    @IBAction func logoTapped(_ sender: Any) {
        
        guard let url = URL(string: opp.url!) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
        
    }
}
