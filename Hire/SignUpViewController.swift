//
//  SignUpViewController.swift
//  Hire
//
//  Created by Skyler Gast on 10/26/20.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func alreadyHaveAccountTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "login")
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        if phone.text?.isEmpty == true {
            print("No phone number entered")
            return
        }
        
        signUp()
    }
    
    func signUp() {
        Auth.auth().createUser(withEmail: phone.text!, password: password.text!) { (authResult, error) in
            guard let user = authResult?.user, error == nil else {
                print("Error \(error?.localizedDescription)")
                return
            }
            self.showHome()
        }
    }
    
    func showHome() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "home")
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true)
    }
}
