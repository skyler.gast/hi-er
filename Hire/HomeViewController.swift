//
//  HomeViewController.swift
//  Hire
//
//  Created by Skyler Gast on 10/26/20.
//

import UIKit
import Firebase
import FirebaseAuth
import MapKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var onlyMatchedSwitch: UISwitch!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var companyCard: UILabel!
    @IBOutlet weak var jobtitleCard: UILabel!
    @IBOutlet weak var descCard: UILabel!
    @IBOutlet weak var logoButton: UIButton!
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var oppView: UIView!
    @IBOutlet weak var profileButton: UIButton!
    
    var opps: Array<Opportunity>! = [Opportunity]()
    
    let db = Firestore.firestore()
    let storage = Storage.storage()
    let firebaseAuth = Auth.auth()
    
    var savedAnnotation: MKAnnotation!
    
    var userProfile: UserProfile!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getOpportunities()
        getUserProfile()
        
        profileButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: profileButton.bounds.width - profileButton.bounds.height)
        profileButton.imageView?.layer.cornerRadius = profileButton.bounds.height/2.0
        
    }
    
    @IBAction func logoTapped(_ sender: Any) {
        showOpportunityDetails()
    }
    
    @IBAction func profileTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "profile") as? ProfileViewController
        present(vc!, animated: true)
    }
    
    
    func updateCard(opp: Opportunity) {
        companyCard.text = opp.title!
        jobtitleCard.text = opp.subtitle!
        descCard.text = opp.shortDesc!
        logoButton.setImage(opp.logo, for: .normal)
        
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        clearOpportunities()
        getOpportunities()
    }
    
    @IBAction func nextOppTapped(_ sender: Any) {
        self.mapView.selectAnnotation(mapView.annotations.randomElement() as! MKAnnotation, animated: true)
    }
    
    @IBAction func prevOppTapped(_ sender: Any) {
    }
    
    @IBAction func onlyMatchedChanged(_ sender: Any) {
        if (onlyMatchedSwitch.isOn) {
            savedAnnotation = mapView.annotations[0]
            mapView.removeAnnotation(mapView.annotations[0])
        } else {
            mapView.addAnnotation(savedAnnotation)
        }
    }
    
    func addPlacemarks(opps: Array<Opportunity>) {
        
        //doesn't appear on the UI until moved.. this code doesn't work
        DispatchQueue.main.async {
            opps.forEach { opp in
                let address = opp.address

                let geoCoder = CLGeocoder()
                geoCoder.geocodeAddressString(address!) { (placemarks, error) in
                        guard
                            let placemarks = placemarks
                        else {
                            // handle no location found
                            return
                        }

                    opp.coordinate.latitude = (placemarks[0].location?.coordinate.latitude)!
                    opp.coordinate.longitude = (placemarks[0].location?.coordinate.longitude)!
                    }
                
                
                self.mapView.addAnnotation(opp)
            }
            
        }
        
        
    }
    
    public func removeAnnotation() {
        mapView.removeAnnotation(self.mapView.selectedAnnotations.first!)
    }
    
    func addOpportunity() {
        var ref: DocumentReference? = nil
        ref = db.collection("opportunities").addDocument(data: [
            "id": "2222",
            "address": "1 Malop Street, Geelong, Victoria 3220",
            "companyName": "WorkSafe",
            "jobTitle": "Project Coordinator",
            "logo": "",
            "longDescription": "The purpose of this position is to provide program support to assist the project manager in the delivery of Worksafe’s strategic initiatives within our Data and Analytics function. The Project Coordinator role is to engage with the PMO, SMEs and Governance committees to assist in the delivery of strategic initiatives. ",
            "shortDescription": "Engage with the PMO, SMEs and Governance committees to assist in the delivery of strategic initiatives.",
            "url": "https://www.worksafe.vic.gov.au/",
            "coordLat": "0",
            "coordLong": "0"
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    func clearOpportunities() {
        self.opps.removeAll()
        self.mapView.removeAnnotations(self.mapView.annotations)
    }
    
    func getUserProfile() {
        let docRef = db.collection("users").document((firebaseAuth.currentUser?.uid.description)!)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let name = document.data()!["firstName"] as? String ?? ""
                
                let profileUrl = document.data()!["profileImage"] as? String ?? ""
                
                let skills = document.data()!["skills"]! as! [String]
                let salary = document.data()!["minSalary"] as! NSNumber
                
                let fullTime = document.data()!["fullTime"] as! Bool
                let travel = document.data()!["travel"] as! Bool
                let remote = document.data()!["remote"] as! Bool

                let applied = document.data()!["applied"]! as! [String]
                let rejected = document.data()!["rejected"]! as! [String]
                
                self.userProfile = UserProfile(id: document.documentID,
                                               name: name,
                                               profileImage: profileUrl,
                                               salary: salary,
                                               skills: skills,
                                               fullTime: fullTime,
                                               remote: remote,
                                               travel: travel,
                                               applied: applied,
                                               rejected: rejected)
                
                self.getProfileImage(profileUrl: profileUrl)
                
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func getOpportunities() {
        
        db.collection("opportunities").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                for document in querySnapshot!.documents {
                    
                    if(self.userProfile.rejected.contains(document.documentID) || self.userProfile.applied.contains(document.documentID)) {
                        continue;
                    }
                    
                    let data = document.data()
                    
                    let id = document.documentID
                    let companyName = data["companyName"] as? String ?? ""
                    let jobTitle = data["jobTitle"] as? String ?? ""
                    let short = data["shortDescription"] as? String ?? ""
                    let long = data["longDescription"] as? String ?? ""
                    let logoUrl = data["logo"] as? String ?? ""
                    let url = data["url"] as? String ?? ""
                    let address = data["address"] as? String ?? ""
                    
                    let o = Opportunity(id: id,
                                       title: companyName,
                                       subtitle: jobTitle,
                                       coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0),
                                       shortDesc: short,
                                       desc: long,
                                       logo: nil,
                                       url: url,
                                       address: address)
                    
                    self.getLogoImage(logoUrl: logoUrl, opp: o)
                    
                    let skills = document.data()["skills"] as? [String]
                    
                    o.skills.append(contentsOf: skills!)
                    
                    self.opps.append(o)
                }
                
                
                self.setupUI()
            }
        }
    }
    
    func getLogoImage(logoUrl: String, opp: Opportunity) {
        let storageRef = storage.reference()
        //var image: UIImage
        // Reference to an image file in Firebase Storage
        let logoRef = storageRef.child(logoUrl)


        // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
        logoRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
          if let error = error {
            // Uh-oh, an error occurred!
            print(error)
            let image = UIImage(named: "silly")
            opp.logo = image
          } else {
            // Data for "images/island.jpg" is returned
            let image = UIImage(data: data!)
            opp.logo = image
          }
            
           
        }
    }
    
    func getProfileImage(profileUrl: String) {
        let storageRef = storage.reference()
        //var image: UIImage
        // Reference to an image file in Firebase Storage
        let logoRef = storageRef.child(profileUrl)


        // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
        logoRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
          if let error = error {
            // Uh-oh, an error occurred!
            print(error)
            let image = UIImage(named: "silly")
            self.profileButton.setImage(image, for: .normal)
            self.profileButton.imageView?.contentMode = .scaleAspectFill
          } else {
            // Data for "images/island.jpg" is returned
            let image = UIImage(data: data!)
            self.profileButton.imageView!.image = image
            self.profileButton.setImage(image, for: .normal)
            self.profileButton.imageView?.contentMode = .scaleAspectFill
          }
            
           
        }
    }
    
    
    func setupUI() {
        let initialLocation = CLLocation(latitude: -38.1499, longitude: 144.3617)
        
        mapView.centerToLocation(initialLocation, regionRadius: 2000)
        
        mapView.delegate = self
        
        addPlacemarks(opps: opps)
        
        oppView.addSubview(CardView())
        companyCard.text = "welcome to hi-er!"
        jobtitleCard.text = ""
        descCard.text = "we found " + opps.count.description + " jobs for you! click below"
        logoButton.setImage(UIImage(named: "hier-logo"), for: .normal)
        
        self.welcomeLabel.text = "hi " + self.userProfile.name + "!"
    }
    
    
    @IBAction func signOutTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "login")
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true)
    }
    
    @IBAction func historyTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "history") as? HistoryViewController
        vc!.opps = self.opps
        present(vc!, animated: true)
    }
    
}

private extension MKMapView {
    func centerToLocation(
        _ location: CLLocation,
        regionRadius: CLLocationDistance = 5000
    ) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius,
            longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}

extension HomeViewController: MKMapViewDelegate {
    
    @objc func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "opportunity"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else {
            view = MKMarkerAnnotationView(
                annotation: annotation,
                reuseIdentifier: identifier)
            view.canShowCallout = true
            //view.calloutOffset = CGPoint(x: -5, y: 5)
            //view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            let details = UIButton(type: .detailDisclosure)
            details.addTarget(self, action: #selector(self.showOpportunityDetails), for: .touchUpInside)
            view.rightCalloutAccessoryView = details
        }
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let pin = (self.mapView.selectedAnnotations.first as? Opportunity)!
        
        updateCard(opp: pin)
    }
      
    @objc func showOpportunityDetails(){
        let pin = self.mapView.selectedAnnotations.first as? Opportunity
        if (pin != nil) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "opportunity") as? OpportunityViewController
            vc?.opportunity = pin
            vc?.userProfile = self.userProfile
            present(vc!, animated: true)
        }
    }
}
