//
//  ProfileViewController.swift
//  Hire
//
//  Created by Skyler Gast on 2/3/21.
//

import Foundation
import UIKit
import FirebaseAuth
import Firebase

class ProfileViewController: UIViewController {
    
    let db = Firestore.firestore()
    let storage = Storage.storage()
    let firebaseAuth = Auth.auth()
    
    @IBOutlet weak var minSalary: UITextField!
    
    @IBOutlet weak var skill1Text: UITextField!
    @IBOutlet weak var skill2Text: UITextField!
    @IBOutlet weak var skill3Text: UITextField!
    @IBOutlet weak var skill4Text: UITextField!
    @IBOutlet weak var skill5Text: UITextField!
    @IBOutlet weak var skill6Text: UITextField!
    @IBOutlet weak var skill7Text: UITextField!
    @IBOutlet weak var skill8Text: UITextField!
    
    @IBOutlet weak var travelImg: UIImageView!
    @IBOutlet weak var remoteImg: UIImageView!
    @IBOutlet weak var fullTimeImg: UIImageView!
    @IBOutlet weak var physicalImg: UIImageView!
    @IBOutlet weak var minHourly: UITextField!
    
    override func viewDidLoad() {
        getProfileInfo()
    }
    
    func getProfileInfo() {
        let docRef = db.collection("users").document((firebaseAuth.currentUser?.uid.description)!)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let skills = document.data()!["skills"]! as! [Any]
                let salary = document.data()!["minSalary"] as! NSNumber
                let hourly = document.data()!["minHourly"] as! NSNumber
                
                let fullTime = document.data()!["fullTime"] as? Bool ?? false
                let travel = document.data()!["travel"] as? Bool ?? false
                let remote = document.data()!["remote"] as? Bool ?? false
                let physical = document.data()!["physical"] as? Bool ?? false
                
                self.minSalary.text = salary.description
                self.minHourly.text = hourly.description
                self.skill1Text.text = skills[0] as? String
                self.skill2Text.text = skills[1] as? String
                self.skill3Text.text = skills[2] as? String
                self.skill4Text.text = skills[3] as? String
                self.skill5Text.text = skills[4] as? String
                self.skill6Text.text = skills[5] as? String
                self.skill7Text.text = skills[6] as? String
                self.skill8Text.text = skills[7] as? String
                
                if (fullTime) {
                    self.fullTimeImg.isHighlighted = true
                }
                if (remote) {
                    self.remoteImg.isHighlighted = true
                }
                if (travel) {
                    self.travelImg.isHighlighted = true
                }
                if (physical) {
                    self.physicalImg.isHighlighted = true
                }
                
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @IBAction func signOutTapped(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "login")
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true)
    }
}

