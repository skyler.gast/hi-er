//
//  OnboardingPrefsView.swift
//  Hire
//
//  Created by Skyler Gast on 3/13/21.
//

import SwiftUI

struct OnboardingPrefsCardView: View {
    
    var body: some View {
        
        let prefs = ["full time", "part time", "remote", "travel", "office", "relocate", "physical", "driving", "hospitality"]
        
        let threeColumnGrid = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
        
        
        VStack {
            Image("quokka-hi-transparent")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 300, alignment: .center)
            Text("how do you like to work?")
                .font(.custom("League Spartan", size: 25))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
            
            ScrollView {
                LazyVGrid(columns: threeColumnGrid, spacing: 10) {
                    ForEach((0...prefs.count-1), id: \.self) { index in
                        Button(action: {
                            print(self)
                        }) {
                            VStack(alignment: .center, spacing: 5.0) {
                                Image(prefs[index])
                                    .resizable()
                                    .scaledToFit()
                                    .cornerRadius(10)
                                    .shadow(color: Color.primary.opacity(0.3), radius: 1)
                                
                                Text(prefs[index])
                                    .foregroundColor(Color("HierDarkBlue"))
                                    .font(.custom("Nunito Sans", size: 20))
                            }
                        }
                    }
                }
            }                        
                
        }.padding()
        
    }
}

struct OnboardingPrefsView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPrefsCardView()
    }
}
