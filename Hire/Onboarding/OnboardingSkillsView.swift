//
//  OnboardingSkillsView.swift
//  Hire
//
//  Created by Skyler Gast on 3/16/21.
//

import SwiftUI

struct OnboardingSkillsView: View {
    
    @State var skillEntered: String = ""
    
    var body: some View {
        
        VStack {
            Image("quokka-hi-transparent")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 300, alignment: .center)
            Text("what are 8 of your job skills?")
                .font(.custom("League Spartan", size: 25))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
            
            TextField("enter a skill", text: $skillEntered)
                
        }.padding()
    }
}

struct OnboardingSkillsView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingSkillsView()
    }
}
