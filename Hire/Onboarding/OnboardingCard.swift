//
//  OnboardingCard.swift
//  Hire
//
//  Created by Skyler Gast on 3/12/21.
//

import Foundation

struct OnboardingCard: Identifiable {
    var id = UUID()
    var image: String
    var title: String
    var description: String
}

var testData: [OnboardingCard] = [
    OnboardingCard(image: "quokka-hi-transparent", title: "how do you like to work?", description: "we need your email and password"),
    OnboardingCard(image: "hier-logo", title: "logo", description: "desc"),
    OnboardingCard(image: "hier-logo", title: "title", description: "desc")
    
]
