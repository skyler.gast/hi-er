//
//  OnboardingLaunchView.swift
//  Hire
//
//  Created by Skyler Gast on 3/14/21.
//

import SwiftUI

struct OnboardingLaunchView: View {
    var body: some View {
   
        VStack {
            Image("quokka-hi-transparent")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 300, alignment: .center)
            Text("let's get you matched!")
                .font(.custom("League Spartan", size: 30))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
                .multilineTextAlignment(.center)
                .padding()
            Text("first tell us how you like to work and some of your skills")
                .font(.custom("League Spartan", size: 25))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
                .multilineTextAlignment(.center)
                .lineLimit(5)
                .padding()
        }.padding()
        //.aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
    }
}

struct OnboardingLaunchView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingLaunchView()
    }
}

