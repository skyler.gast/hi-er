//
//  OnboardingViewController.swift
//  Hire
//
//  Created by Skyler Gast on 3/13/21.
//

import UIKit
import SwiftUI

class OnboardingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        

        let swiftUIView = OnboardingView(dismissAction: { self.dismiss( animated: true, completion: nil) })
        let hostingController = UIHostingController.init(rootView: swiftUIView)
        self.addChild(hostingController)
        hostingController.didMove(toParent: self)
        self.view.addSubview(hostingController.view)
        
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            hostingController.view.topAnchor.constraint(equalTo: self.view.topAnchor),
            hostingController.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            hostingController.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            hostingController.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
       
    }

}
