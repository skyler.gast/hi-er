//
//  OnboardingPersonalView.swift
//  Hire
//
//  Created by Skyler Gast on 4/8/21.
//

import SwiftUI

struct OnboardingPersonalView: View {
    @State var hourly: String = ""
    @State var salary: String = ""
    
    
    var body: some View {
        VStack {
//            Image("quokka-hi-transparent")
//                .resizable()
//                .aspectRatio(contentMode: .fill)
//                .frame(width: 300, height: 300, alignment: .center)
            Text("just a few more questions")
                .font(.custom("League Spartan", size: 25))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
                .multilineTextAlignment(.center)
                .padding()
            Text("what is your minimum acceptable hourly rate?")
                .font(.custom("Nunito Sans", size: 22))
                .foregroundColor(Color("HierDarkBlue"))
            TextField("$XXX.XX", text: $hourly)
            Text("what is your minimum acceptable annual salary?")
                .font(.custom("Nunito Sans", size: 22))
                .foregroundColor(Color("HierDarkBlue"))
            TextField("$XXX,XXX", text: $salary)
               
        }.padding()
    }
}

struct OnboardingPersonalView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingPersonalView()
    }
}
