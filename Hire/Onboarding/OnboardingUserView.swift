//
//  OnboardingUserView.swift
//  Hire
//
//  Created by Skyler Gast on 4/8/21.
//

import SwiftUI

struct OnboardingUserView: View {
    @State var username: String = ""
    @State var password: String = ""
    
    var body: some View {
        VStack {
//            Image("quokka-hi-transparent")
//                .resizable()
//                .aspectRatio(contentMode: .fill)
//                .frame(width: 300, height: 300, alignment: .center)
            Text("just a few more questions")
                .font(.custom("League Spartan", size: 25))
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
                .multilineTextAlignment(.center)
                .padding()
            Text("enter your email")
                .font(.custom("Nunito Sans", size: 22))
                .foregroundColor(Color("HierDarkBlue"))
            TextField("email address", text: $username)
            Text("choose a password")
                .font(.custom("Nunito Sans", size: 22))
                .foregroundColor(Color("HierDarkBlue"))
            TextField("password", text: $password)
               
        }.padding()
    }
}

struct OnboardingUserView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingUserView()
    }
}
