//
//  OnboardingCardView.swift
//  Hire
//
//  Created by Skyler Gast on 3/12/21.
//

import SwiftUI

struct OnboardingCardView: View {
    
    var card: OnboardingCard
    
    var body: some View {
        VStack {
            Image(card.image).resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 300, height: 300, alignment: .center)
            Text(card.title)
                .font(.body)
                .fontWeight(.bold)
                .foregroundColor(Color("HierDarkBlue"))
            Text(card.description)
                .lineLimit(5)
                .multilineTextAlignment(.center)
                .font(.body)
                .foregroundColor(Color("HierDarkBlue"))
                .padding()
        }.padding()
        
    }
}

struct OnboardingCardView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingCardView(card: testData[1])
    }
}
