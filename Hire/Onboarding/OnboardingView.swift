//
//  OnboardingView.swift
//  Hire
//
//  Created by Skyler Gast on 3/12/21.
//

import SwiftUI



struct OnboardingView: View {
    
    @State var selectedPage: Int = 0
    @State var remote = true
    
    var dismissAction: (() -> Void)?
    
    func savePrefs(remote: Bool) {
        self.remote = remote
    }
    
    
    
    
    var body: some View {
        
        ScrollView {
            
            TabView (selection: $selectedPage) {
                
                VStack {
                    OnboardingLaunchView()
                    
                    Button(action: {
                        self.selectedPage = 1
                    }) {
                        HStack {
    //                        Image(systemName: "arrow.right")
                            Text("get started")
                                .font(.custom("Nunito Sans", size: 24))
                        }
                    }
                    .foregroundColor(Color.white)
                    .padding()
                    .background(Color("HierDarkBlue"))
                    .cornerRadius(10.0)
                }.tag(0)
                
                VStack {
                    OnboardingPrefsCardView()
                    Button(action: {
                        self.selectedPage = 2
                    }) {
                        HStack {
                            Image(systemName: "arrow.right")
                            Text("enter my skills")
                                .font(.custom("Nunito Sans", size: 24))
                        }
                    }
                    .foregroundColor(Color.white)
                    .padding()
                    .background(Color("HierDarkBlue"))
                    .cornerRadius(10.0)
                }.tag(1)
                
                VStack {
                    OnboardingSkillsView()
                    Button(action: {
                        self.selectedPage = 3
                    }) {
                        HStack {
                            Image(systemName: "arrow.right")
                            Text("enter my skills")
                                .font(.custom("Nunito Sans", size: 24))
                        }
                    }
                    .foregroundColor(Color.white)
                    .padding()
                    .background(Color("HierDarkBlue"))
                    .cornerRadius(10.0)
                }.tag(2)
                
                VStack {
                    OnboardingPersonalView()
                    Button(action: {
                        self.selectedPage = 4
                    }) {
                        HStack {
                            Image(systemName: "arrow.right")
                            Text("finish up")
                                .font(.custom("Nunito Sans", size: 24))
                        }
                    }
                    .foregroundColor(Color.white)
                    .padding()
                    .background(Color("HierDarkBlue"))
                    .cornerRadius(10.0)
                }.tag(3)
                
                VStack {
                    OnboardingUserView()
                    Button(action: dismissAction!) {
                        HStack {
                            Image(systemName: "hand.thumbsup")
                            Text("login and start looking!")
                                .font(.custom("Nunito Sans", size: 24))
                        }
                    }
                    .foregroundColor(Color.white)
                    .padding()
                    .background(Color("HierDarkBlue"))
                    .cornerRadius(10.0)
                }.tag(4)
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            .background(Color("HierTeal"))
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        }.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
