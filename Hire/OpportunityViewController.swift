//
//  OpportunityViewController.swift
//  Hire
//
//  Created by Skyler Gast on 1/4/21.
//

import UIKit
import Firebase
import FirebaseAuth
import MapKit

class OpportunityViewController: UIViewController {
    var opportunity: Opportunity!
    var userProfile: UserProfile!
    let db = Firestore.firestore()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var matchedOnLabel: UILabel!
    @IBOutlet weak var matchedTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.companyLabel.text = opportunity.title
        self.titleLabel.text = opportunity.subtitle
        self.descriptionLabel.text = opportunity.desc
        self.companyImage.image = opportunity.logo
        
        let matches = userProfile.skills.filter(opportunity.skills.contains)
        
        if (matches.count == 0) {
            self.matchedTitle.text = "No skills matched"
            self.matchedOnLabel.text = ""
        }
        else {
            self.matchedTitle.text = "Matched On"
            self.matchedOnLabel.text = matches.joined(separator: ", ")
        }
        
        
        
    }
    
    @IBAction func thumbsUpTapped(_ sender: Any) {        
        opportunity.hidden = true
        opportunity.matchedCandidate = true
        
        let docRef = db.collection("users").document(userProfile.id)
        docRef.updateData([
            "applied": FieldValue.arrayUnion([opportunity.id])
        ])
        
        let docRefOpp = db.collection("opportunities").document(opportunity.id ?? "missingId")
        docRefOpp.updateData([
            "appliedUsers": FieldValue.arrayUnion([userProfile.id])
        ])
        
        let pvc = self.presentingViewController
        
        self.dismiss(animated: false) { [self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "success") as SuccessViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.opp = opportunity
            pvc?.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
    
    @IBAction func thumbsDownTapped(_ sender: Any) {
        opportunity.hidden = true
        opportunity.matchedCandidate = false
        
        let docRef = db.collection("users").document(userProfile.id)
        docRef.updateData([
            "rejected": FieldValue.arrayUnion([opportunity.id])
        ])
        
        self.dismiss(animated: true)
    }
}
