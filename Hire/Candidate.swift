//
//  Candidate.swift
//  Hire
//
//  Created by Skyler Gast on 1/18/21.
//

import Foundation

class Candidate: NSObject {
    let name: String?

    
    init(
        name: String?
    ) {
        self.name = name
        
        super.init()
    }
    
}
